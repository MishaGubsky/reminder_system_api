# Reminder API application (Django REST)

## Development:
### Local setup

1. install docker and docker-compose
    - https://docs.docker.com/install/#server
    - https://docs.docker.com/compose/install/
2. create `.env` according to `.env.example`
3. create and build containers: `docker-compose build`
4. run migrations: `docker-compose run api ./manage.py migrate`
5. run containers: `docker-compose up`

### To receive email messages locally

The mailcatcher is used as simple local smpt server.
You can update settings in`.env` file.

- To view message go to `http://127.0.0.1:1080/`

You can get more detailed describtion here:
https://mailcatcher.me/
