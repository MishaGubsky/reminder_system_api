import pytz
from datetime import datetime
from celery.concurrency import thread

from reminder_system_api.celery import app as celery_app
from reminder_system_api.utils import chunk_queryset_iterator

from reminders.models import Reminder, UserReminder
from reminders.mailer import ReminderMailer

BATCH_SIZE = 100


@celery_app.task
def async_distribute_reminder_to_users(reminder_id):
    RemindersDistributionService.distribute_reminder(reminder_id)


class RemindersDistributionService:
    @classmethod
    def _get_queryset(cls):
        return Reminder.objects.filter(
            user_reminders__status=UserReminder.NOT_DELIVERED, remind_at__lte=datetime.now(tz=pytz.utc)
        ).distinct()

    @classmethod
    def _deliver_reminder_to_user(cls, reminder, recipient):
        success = ReminderMailer.send_reminder({"reminder": reminder, "email": recipient.email})
        status = UserReminder.DELIVERED if success else UserReminder.NOT_DELIVERED
        UserReminder.objects.filter(reminder_id=reminder, recipient_id=recipient.id).update(status=status)

    @classmethod
    def _deliver_reminder(cls, reminder):
        recipients_ids = list(reminder.recipients.filter(
            user_reminders__status=UserReminder.NOT_DELIVERED).values_list('id', flat=True))
        recipient_query = reminder.recipients.filter(id__in=recipients_ids).order_by('id')

        reminder.user_reminders.filter(status=UserReminder.NOT_DELIVERED).update(status=UserReminder.IN_PROGRESS)

        pool = thread.TaskPool()
        for recipient in chunk_queryset_iterator(recipient_query, BATCH_SIZE):
            pool.on_apply(cls._deliver_reminder_to_user, (reminder, recipient))

        pool.stop()

    @classmethod
    def distribute_reminders(cls):
        reminders = cls._get_queryset()
        for reminder in reminders:
            async_distribute_reminder_to_users.apply_async(args=(reminder.id,))

    @classmethod
    def distribute_reminder(cls, reminder_id):
        reminder = Reminder.objects.filter(id=reminder_id).first()

        if not reminder:
            print("Reminder not found in distribution task with ID %s " % reminder_id)
            return

        cls._deliver_reminder(reminder)
