from reminder_system_api.celery import app as celery_app
from reminders.services import RemindersDistributionService


@celery_app.task
def distribute_reminders():
    RemindersDistributionService.distribute_reminders()
