from django.db import models

from users.models import User

from .remider import Reminder


class UserReminder(models.Model):
    NOT_DELIVERED = 'not_delivered'
    IN_PROGRESS = 'in_progress'
    DELIVERED = 'delivered'
    CANCELED = 'canceled'

    STATUS_CHOICES = (
        (NOT_DELIVERED, 'Not delivered'),
        (IN_PROGRESS, 'In progress'),
        (DELIVERED, 'Delivered'),
        (CANCELED, 'Canceled'),
    )

    NOT_DONE_STATUSES = (NOT_DELIVERED, IN_PROGRESS)
    DONE_STATUSES = (DELIVERED, CANCELED)

    status = models.CharField(max_length=255, choices=STATUS_CHOICES, default=NOT_DELIVERED)
    is_invitation = models.BooleanField(default=True)

    reminder = models.ForeignKey(Reminder, on_delete=models.CASCADE, related_name='user_reminders')
    recipient = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_reminders')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
