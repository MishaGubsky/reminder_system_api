from django.db import models


class Reminder(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    place = models.CharField(max_length=255, blank=True)
    remind_at = models.DateTimeField()

    recipients = models.ManyToManyField('users.User', through='UserReminder', related_name='reminders')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
