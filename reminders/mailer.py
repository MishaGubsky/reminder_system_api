from django.template.loader import get_template

from reminder_system_api.utils.base_mailer import BaseMailer


class ReminderMailer(BaseMailer):
    @classmethod
    def send_reminder(cls, data):
        subject = data.get('reminder').title
        body = get_template('email_templates/reminder_template.html').render(data)
        return cls._send_email([data.get('email')], subject, body)
