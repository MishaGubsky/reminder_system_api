from django.utils.translation import ugettext as _
from rest_framework import serializers
from django.db import transaction
from datetime import datetime
import pytz

from users.models import User

from reminders.models import Reminder, UserReminder


class ReminderSerializer(serializers.ModelSerializer):
    added_recipient_ids = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(), many=True, write_only=True, required=False)
    removed_recipient_ids = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(), many=True, write_only=True, required=False)
    status = serializers.CharField(write_only=True, required=False)
    is_invitation = serializers.SerializerMethodField()
    is_editable = serializers.SerializerMethodField()
    is_done = serializers.SerializerMethodField()

    class Meta:
        model = Reminder
        exclude = ('updated_at', 'recipients',)
        extra_kwargs = {
            'title': {'required': False},
            'remind_at': {'required': False},
        }

    def to_internal_value(self, data):
        field = self.fields['remind_at']

        if data.get('remind_at'):
            remind_at = field.to_internal_value(data.get('remind_at')).replace(second=0, microsecond=0)
            data['remind_at'] = field.to_representation(remind_at)

        if data.get('done') is not None:
            data['status'] = UserReminder.CANCELED if data.pop('done') else UserReminder.NOT_DELIVERED

        return super().to_internal_value(data)

    def to_representation(self, value):
        value.creator_reminder = value.user_reminders.filter(recipient=self.context['request'].user).last()
        return super().to_representation(value)

    def validate(self, data):
        if self.instance and datetime.now(tz=pytz.utc) >= self.instance.remind_at:
            raise serializers.ValidationError({'remind_at': _('Edit date is already passed')})

        return super().validate(data)

    def get_is_done(self, instance):
        return instance.creator_reminder.status in UserReminder.DONE_STATUSES

    def get_status(self, instance):
        return instance.creator_reminder.status

    def get_is_invitation(self, instance):
        return instance.creator_reminder.is_invitation

    def get_is_editable(self, instance):
        return datetime.now(tz=pytz.utc) < instance.remind_at

    def validate_remind_at(self, value):
        if datetime.now(tz=pytz.utc) >= value:
            raise serializers.ValidationError(_('Date should be in future'))

        return value

    @transaction.atomic
    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['recipients'] = validated_data.pop('added_recipient_ids', [])
        validated_data['recipients'].append(user)
        reminder = super().create(validated_data)
        reminder.user_reminders.filter(recipient=user).update(is_invitation=False)
        return reminder

    @transaction.atomic
    def update(self, instance, validated_data):
        status = validated_data.pop('status', None)
        if status is not None:
            user = self.context['request'].user
            instance.user_reminders.filter(recipient=user).update(status=status)

        removed_recipient_ids = validated_data.pop('removed_recipient_ids', [])
        added_recipient_ids = validated_data.pop('added_recipient_ids', [])

        reminder = super().update(instance, validated_data)

        reminder.user_reminders.filter(recipient_id__in=removed_recipient_ids).delete()
        reminder.recipients.add(*added_recipient_ids)

        return reminder
