# Generated by Django 3.0.4 on 2020-03-18 10:41

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Reminder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True)),
                ('place', models.CharField(max_length=255)),
                ('remind_at', models.DateTimeField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserReminder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_done', models.BooleanField(default=False)),
                ('is_invitation', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('recipient', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_reminders', to=settings.AUTH_USER_MODEL)),
                ('reminder', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_reminders', to='reminders.Reminder')),
            ],
        ),
        migrations.AddField(
            model_name='reminder',
            name='recipients',
            field=models.ManyToManyField(related_name='reminders', through='reminders.UserReminder', to=settings.AUTH_USER_MODEL),
        ),
    ]
