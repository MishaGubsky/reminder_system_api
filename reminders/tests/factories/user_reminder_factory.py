import random
import factory
from faker import Factory

from reminders.models import UserReminder
from users.tests.factories import UserFactory

from .reminder_factory import ReminderFactory


faker = Factory.create()


class UserReminderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserReminder

    status = factory.Iterator([k for k, v in UserReminder.STATUS_CHOICES])
    is_invitation = True
    recipient = factory.SubFactory(UserFactory)
    reminder = factory.SubFactory(ReminderFactory)


class UserWithOwnReminderFactory(UserFactory):
    owner = factory.RelatedFactory(
        UserReminderFactory,
        factory_related_name='recipient',
        is_invitation=False,
    )


class ReminderWithRecipientsFactory(ReminderFactory):
    owner = factory.RelatedFactory(
        UserReminderFactory,
        factory_related_name='reminder',
        is_invitation=False,
    )
    invited_recipients = factory.RelatedFactoryList(
        UserReminderFactory,
        factory_related_name='reminder',
        size=lambda: random.randint(1, 3),
    )
