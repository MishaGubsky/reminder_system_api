import factory
from faker import Factory

from reminders.models import Reminder

faker = Factory.create()


class ReminderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Reminder

    title = faker.sentence()
    description = faker.text()
    place = faker.address()
    remind_at = faker.date_time()
