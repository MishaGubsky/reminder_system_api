from django_filters.filters import OrderingFilter
from django_filters import FilterSet, BooleanFilter

from reminders.models import Reminder, UserReminder


class RemindersFilterSet(FilterSet):
    is_invitation = BooleanFilter(field_name='user_reminders__is_invitation', method='filter_is_invitation')
    is_done = BooleanFilter(method='filter_is_done')

    ordering = OrderingFilter(
        fields=(
            'created_at',
            'remind_at',
            'title',
            'place',
        )
    )

    class Meta:
        model = Reminder
        fields = ['is_invitation', 'is_done']

    def filter_is_invitation(self, queryset, name, value):
        return queryset.filter(user_reminders__is_invitation=value, user_reminders__recipient=self.request.user)

    def filter_is_done(self, queryset, name, value):
        statuses = UserReminder.DONE_STATUSES if value else UserReminder.NOT_DONE_STATUSES
        return queryset.filter(user_reminders__status__in=statuses, user_reminders__recipient=self.request.user)
