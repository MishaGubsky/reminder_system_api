from rest_framework import generics

from reminders.serializers import ReminderSerializer


class ReminderRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ReminderSerializer

    def get_queryset(self):
        return self.request.user.reminders.all()
