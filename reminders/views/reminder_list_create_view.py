from rest_framework import generics

from reminders.serializers import ReminderSerializer

from reminders.filters import RemindersFilterSet


class ReminderListCreateView(generics.ListCreateAPIView):
    serializer_class = ReminderSerializer
    filterset_class = RemindersFilterSet

    def get_queryset(self):
        return self.request.user.reminders.order_by('-created_at')
