from django.conf.urls import url

from reminders.views import ReminderListCreateView, ReminderRetrieveUpdateDestroyView

urlpatterns = [
    url(r'^reminders/(?P<pk>\d+)/$', ReminderRetrieveUpdateDestroyView.as_view(), name='reminder'),
    url(r'^reminders/$', ReminderListCreateView.as_view(), name='reminders'),
]
