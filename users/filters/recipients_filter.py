from rest_framework.filters import BaseFilterBackend
from django.db.models import Q


class RecipientsFilterBackend(BaseFilterBackend):
    def filter_by_reminder_id(self, request):
        reminder_id = request.query_params.get('reminder_id')
        if reminder_id:
            return Q(reminders__id=reminder_id)

        return Q()

    def exclude_by_reminder_id(self, request):
        reminder_id = request.query_params.get('exclude_reminder_id')
        if reminder_id:
            return ~Q(reminders__id=reminder_id)

        return Q()

    def get_query(self, request):
        query = Q()

        query &= self.filter_by_reminder_id(request)
        query &= self.exclude_by_reminder_id(request)

        return query

    def filter_queryset(self, request, queryset, view):
        return queryset.filter(self.get_query(request))
