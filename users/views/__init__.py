from .recipients_list_view import RecipientsListView
from .current_user_view import CurrentUserView

__all__ = ['CurrentUserView', 'RecipientsListView']
