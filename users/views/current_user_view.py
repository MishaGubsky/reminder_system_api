from rest_framework.generics import RetrieveUpdateAPIView

from users.serializers import UserEditSerializer, UserSerializer


class CurrentUserView(RetrieveUpdateAPIView):
    def get_object(self):
        return self.request.user

    def get_serializer_class(self):
        return UserSerializer if self.request.method == 'GET' else UserEditSerializer
