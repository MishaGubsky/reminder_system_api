from rest_framework.filters import OrderingFilter
from rest_framework.generics import ListAPIView

from users.filters import RecipientsFilterBackend
from users.serializers import UserSerializer
from users.models import User


class RecipientsListView(ListAPIView):
    serializer_class = UserSerializer
    filter_backends = (RecipientsFilterBackend, OrderingFilter,)
    ordering_fields = ('username', 'first_name', 'last_name', 'email',)
    ordering = ('username',)

    def get_queryset(self):
        return User.objects.exclude(id=self.request.user.id)
