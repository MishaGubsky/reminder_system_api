from django.utils import timezone
from rest_framework import status
from django.test import TestCase
from django.urls import reverse

from users.models import User
from users.serializers import UserSerializer


class BaseTestCase(TestCase):
    def setUp(self):
        self.data = {
            'identity': 'test',
            'password': 'password',
        }
        self.user = User(username=self.data['identity'])
        self.user.set_password(self.data['password'])
        self.user.save()


class ViewLimitationsTestCase(TestCase):
    def test_http_method_not_exist(self):
        methods = ['get', 'put', 'patch', 'delete']
        for method in methods:
            client_method = getattr(self.client, method)
            response = client_method(reverse('sign_in'))
            self.assertEqual(response.data, {"detail": f"Method \"{method.upper()}\" not allowed."})
            self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class InvalidPostRequestTestCase(BaseTestCase):
    def _check_view_error(self, data, field, message, http_code=400, use_double_quotes=False):
        response = self.client.post(reverse('sign_in'), data)
        response_data = response.json()
        self.assertTrue(response.status_code, http_code)
        self.assertTrue('errors' in response_data)
        self.assertTrue(field in response_data['errors'])
        string = f'[\"{message}\"]' if use_double_quotes else f'[\'{message}\']'
        self.assertEqual(str(response_data['errors'][field]), string)

    def test_error_with_identity_absent(self):
        del self.data['identity']
        self._check_view_error(self.data, 'identity', 'This field is required.')

    def test_error_with_password_absent(self):
        del self.data['password']
        self._check_view_error(self.data, 'password', 'This field is required.')

    def test_error_invalid_identity(self):
        self.data['identity'] = 'test1'
        self._check_view_error(self.data, 'password', 'Login or password is incorrect')

    def test_error_invalid_password(self):
        self.data['password'] = 'password1'
        self._check_view_error(self.data, 'password', 'Login or password is incorrect')


class SuccessPostRequestTestCase(BaseTestCase):
    def _make_request(self):
        response = self.client.post(reverse('sign_in'), self.data)
        self.assertTrue(response.status_code, 200)
        return response.json()

    def test_success_response(self):
        response_data = self._make_request()
        for field in ['refresh', 'access', 'user']:
            self.assertIn(field, response_data)

        self.assertTrue(isinstance(response_data['refresh'], str))
        self.assertTrue(isinstance(response_data['access'], str))
        self.assertTrue(isinstance(response_data['user'], dict))

        self.assertDictEqual(response_data['user'], UserSerializer(self.user).data)

    def test_user_last_login_was_updated(self):
        last_login = timezone.now()
        self.user.last_login = last_login
        self.user.save()

        self._make_request()

        user = User.objects.filter(id=self.user.id).last()
        self.assertTrue(last_login < user.last_login)
