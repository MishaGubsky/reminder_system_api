from rest_framework_simplejwt.settings import api_settings
from rest_framework_simplejwt.tokens import AccessToken
from django.test import RequestFactory
from rest_framework import status
from django.urls import reverse
from datetime import timedelta

from reminders.tests.factories import ReminderWithRecipientsFactory
from users.views.recipients_list_view import RecipientsListView
from reminder_system_api.utils.tests import APIViewTestCase
from users.tests.factories import UserFactory


class BaseTestCase(APIViewTestCase):
    view_class = RecipientsListView
    view_name = 'recipients'

    def setUp(self):
        self.other_users = UserFactory.create_batch(3)
        self.user = UserFactory.create()

    def _make_auth_request(self, lifetime=None, **kwargs):
        token = AccessToken.for_user(self.user)
        token.set_exp(lifetime=lifetime)
        self.authenticate_with_token(api_settings.AUTH_HEADER_TYPES[0], token)
        return self.view_get(**kwargs)

    def _get_user_ids(self, users, transform=None, order_field=None):
        transform = transform or (lambda x: getattr(x, 'id'))
        if order_field:
            reverse = order_field[0] == '-'
            if reverse:
                order_field = order_field[1:]

            users.sort(key=lambda x: getattr(x, order_field), reverse=reverse)

        return list(map(transform, users))


class ViewLimitationsTestCase(BaseTestCase):
    def test_http_method_not_allowed(self):
        methods = ['post', 'delete', 'put', 'patch']
        for method in methods:
            token = AccessToken.for_user(self.user)
            self.authenticate_with_token(api_settings.AUTH_HEADER_TYPES[0], token)
            response = getattr(self, f'view_{method}')()
            self.assertEqual(response.data, {"detail": f"Method \"{method.upper()}\" not allowed."})
            self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_not_authenticated(self):
        response = self.view_get()
        self.assertEqual(response.status_code, 401)
        self.assertIn('credentials were not provided', response.data['detail'])

    def test_authorizated(self):
        response = self._make_auth_request()
        self.assertEqual(response.status_code, 200)

    def test_expired_token(self):
        response = self._make_auth_request(lifetime=timedelta(seconds=0))
        self.assertEqual(response.status_code, 401)
        self.assertEqual('token_not_valid', response.data['code'])


class GetQuerysetTestCase(BaseTestCase):
    def test_return_request_user(self):
        request = RequestFactory().get(reverse(self.view_name))
        request.user = self.user
        view = self.view_class(request=request)
        other_user_ids = self._get_user_ids(self.other_users)
        self.assertQuerysetEqual(
            view.get_queryset(), other_user_ids,
            transform=lambda x: x.id, ordered=False)


class PaginationTestCase(BaseTestCase):
    def test_success_response(self):
        response = self._make_auth_request()
        response_data = response.json()
        for field in ['count', 'next', 'previous', 'results']:
            self.assertIn(field, response_data)

        self.assertTrue(isinstance(response_data['count'], int))
        self.assertTrue(isinstance(response_data['results'], list))

    def test_next_field_type_in_response(self):
        UserFactory.create_batch(self.view_class.pagination_class.page_size)
        response = self._make_auth_request()
        response_data = response.json()
        self.assertTrue(isinstance(response_data['next'], dict))
        self.assertDictEqual(response_data['next'], {'page': 2})

    def test_previous_field_type_in_response(self):
        UserFactory.create_batch(self.view_class.pagination_class.page_size)
        response = self._make_auth_request(query_string='page=2')
        response_data = response.json()
        self.assertTrue(isinstance(response_data['previous'], dict))
        self.assertDictEqual(response_data['previous'], {'page': 1})


class ViewOrderingTestCase(BaseTestCase):
    def _check_response_order(self, ordering):
        response = self._make_auth_request(query_string=f'ordering={ordering}')
        response_user_ids = self._get_user_ids(response.json()['results'], lambda x: x['id'])
        other_user_ids = self._get_user_ids(self.other_users, order_field=ordering)
        self.assertEqual(response_user_ids, other_user_ids)

    def test_default_ordering(self):
        self._check_response_order('username')
        self._check_response_order('-username')

    def test_first_name_ordering(self):
        self._check_response_order('first_name')
        self._check_response_order('-first_name')

    def test_last_name_ordering(self):
        self._check_response_order('last_name')
        self._check_response_order('-last_name')

    def test_email_ordering(self):
        self._check_response_order('email')
        self._check_response_order('-email')


class ViewFilteringTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.reminder = ReminderWithRecipientsFactory(owner__recipient=self.user)

    def _compare_users(self, response, users):
        response_user_ids = self._get_user_ids(response.json()['results'], lambda x: x['id'])
        other_user_ids = self._get_user_ids(users, lambda x: str(getattr(x, 'id')))
        self.assertQuerysetEqual(response_user_ids, other_user_ids, ordered=False)

    def test_get_invited_recipients(self):
        response = self._make_auth_request(query_string=f'reminder_id={self.reminder.id}')
        recipients = self.reminder.recipients.filter(user_reminders__is_invitation=True)
        self._compare_users(response, recipients)

    def test_get_free_recipients(self):
        response = self._make_auth_request(query_string=f'exclude_reminder_id={self.reminder.id}')
        self._compare_users(response, self.other_users)

    def test_get_free_recipients_for_new_reminder(self):
        response = self._make_auth_request()
        invited_recipients = self.reminder.recipients.filter(user_reminders__is_invitation=True)
        self._compare_users(response, list(invited_recipients) + self.other_users)
