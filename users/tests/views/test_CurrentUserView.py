from rest_framework_simplejwt.settings import api_settings
from rest_framework_simplejwt.tokens import AccessToken
from django.test import RequestFactory
from rest_framework import status
from django.urls import reverse
from datetime import timedelta

from users.serializers import UserSerializer, UserEditSerializer
from reminder_system_api.utils.tests import APIViewTestCase
from users.views.current_user_view import CurrentUserView
from users.models import User


class BaseTestCase(APIViewTestCase):
    view_class = CurrentUserView
    view_name = 'current_user'

    def setUp(self):
        self.data = {
            'username': 'test',
            'password': 'password',
        }
        self.user = User.objects.create(**self.data)

    def _authenticate_client(self, lifetime=None):
        token = AccessToken.for_user(self.user)
        token.set_exp(lifetime=lifetime)
        self.authenticate_with_token(api_settings.AUTH_HEADER_TYPES[0], token)

    def _make_auth_get_request(self, lifetime=None):
        self._authenticate_client(lifetime)
        return self.view_get()


class ViewLimitationsTestCase(BaseTestCase):
    def test_http_method_not_allowed(self):
        methods = ['post', 'delete']
        for method in methods:
            self._authenticate_client()
            response = getattr(self, f'view_{method}')()
            self.assertEqual(response.data, {"detail": f"Method \"{method.upper()}\" not allowed."})
            self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_not_authenticated(self):
        response = self.view_get()
        self.assertEqual(response.status_code, 401)
        self.assertIn('credentials were not provided', response.data['detail'])

    def test_authorizated(self):
        response = self._make_auth_get_request()
        self.assertEqual(response.status_code, 200)

    def test_expired_token(self):
        response = self._make_auth_get_request(lifetime=timedelta(seconds=0))
        self.assertEqual(response.status_code, 401)
        self.assertEqual('token_not_valid', response.data['code'])
        # self.assertIn('credentials were not provided', res.data['detail'])


class GetObjectTestCase(BaseTestCase):
    def test_return_request_user(self):
        request = RequestFactory().get(reverse(self.view_name))
        request.user = self.user
        view = self.view_class(request=request)
        self.assertEqual(view.get_object().id, self.user.id)


class GetSerializerClassTestCase(BaseTestCase):
    def test_return_user_serializer(self):
        request = RequestFactory().get(reverse(self.view_name))
        view = self.view_class(request=request)
        self.assertEqual(view.get_serializer_class(), UserSerializer)

    def test_return_user_edit_serializer_using_put(self):
        request = RequestFactory().put(reverse(self.view_name))
        view = self.view_class(request=request)
        self.assertEqual(view.get_serializer_class(), UserEditSerializer)

    def test_return_user_edit_serializer_using_patch(self):
        request = RequestFactory().patch(reverse(self.view_name))
        view = self.view_class(request=request)
        self.assertEqual(view.get_serializer_class(), UserEditSerializer)


class SuccessGetRequestTestCase(BaseTestCase):
    def _make_request(self):
        response = self._make_auth_get_request()
        self.assertTrue(response.status_code, 200)
        return response.json()

    def test_success_response(self):
        response_data = self._make_request()
        self.assertDictEqual(response_data, UserSerializer(self.user).data)


class SuccessPatchRequestTestCase(BaseTestCase):
    def test_success_response(self):
        self._authenticate_client()
        data = {'first_name': 'first name'}
        response_data = self.view_patch(data).json()
        self.user.first_name = data['first_name']
        self.assertDictEqual(response_data, UserSerializer(self.user).data)
