from rest_framework import status
from django.test import TestCase
from django.urls import reverse

from users.models import User
from users.serializers import UserSerializer


class BaseTestCase(TestCase):
    def setUp(self):
        self.data = {
            'username': 'test',
            'email': 'test@test.test',
            'password': 'password12345678',
            'password_confirmation': 'password12345678',
        }

    def _create_user(self):
        data = self.data.copy()
        data.pop('password_confirmation')
        self.user = User.objects.create(**data)


class ViewLimitationsTestCase(TestCase):
    def test_http_method_not_exist(self):
        methods = ['get', 'put', 'patch', 'delete']
        for method in methods:
            client_method = getattr(self.client, method)
            response = client_method(reverse('sign_up'))
            self.assertEqual(response.data, {"detail": f"Method \"{method.upper()}\" not allowed."})
            self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class InvalidPostRequestTestCase(BaseTestCase):
    def _check_view_error(self, field, message, http_code=400, use_double_quotes=False):
        response = self.client.post(reverse('sign_up'), self.data)
        response_data = response.json()
        self.assertTrue(response.status_code, http_code)
        self.assertTrue('errors' in response_data)
        self.assertTrue(field in response_data['errors'])
        string = f'[\"{message}\"]' if use_double_quotes else f'[\'{message}\']'
        self.assertEqual(str(response_data['errors'][field]), string)

    def test_error_with_username_absent(self):
        del self.data['username']
        self._check_view_error('username', 'This field is required.')

    def test_error_with_email_absent(self):
        del self.data['email']
        self._check_view_error('email', 'This field is required.')

    def test_error_with_password_absent(self):
        del self.data['password']
        self._check_view_error('password', 'This field is required.')

    def test_error_with_password_confirmation_absent(self):
        del self.data['password_confirmation']
        self._check_view_error('password_confirmation', 'This field is required.')

    def test_error_with_with_too_common_password_identity(self):
        self.data['password'] = 'password'
        self.data['password_confirmation'] = 'password'
        self._check_view_error('password', 'This password is too common.')

    def test_error_with_unmatched_passwords_identity(self):
        self.data['password'] = 'password'
        self._check_view_error('password_confirmation', "Passwords don't match", use_double_quotes=True)

    def test_error_with_username_uniqueness(self):
        self._create_user()
        self._check_view_error('username', 'A user with that username already exists.')

    def test_error_with_email_uniqueness(self):
        self._create_user()
        self._check_view_error('email', 'user with this email address already exists.')


class SuccessPostRequestTestCase(BaseTestCase):
    def _make_request(self):
        response = self.client.post(reverse('sign_up'), self.data)
        self.assertTrue(response.status_code, 200)

        self.user = User.objects.filter(username=self.data['username']).last()

        return response.json()

    def test_user_created(self):
        count = int(User.objects.count())
        self._make_request()
        self.assertEqual(User.objects.count(), count + 1)
        self.assertTrue(self.user.check_password(self.data['password']))

    def test_success_response(self):
        response_data = self._make_request()
        for field in ['refresh', 'access', 'user']:
            self.assertIn(field, response_data)

        self.assertTrue(isinstance(response_data['refresh'], str))
        self.assertTrue(isinstance(response_data['access'], str))
        self.assertTrue(isinstance(response_data['user'], dict))

        self.assertDictEqual(response_data['user'], UserSerializer(self.user).data)

    def test_user_last_login_was_updated(self):
        self._make_request()
        self.assertFalse(self.user.last_login is None)
