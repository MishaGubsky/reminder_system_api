from django.test import RequestFactory
from unittest.mock import patch
from django.urls import reverse
from django.db.models import Q

from reminder_system_api.utils.tests import APIViewTestCase
from users.filters import RecipientsFilterBackend
from users.models import User


class BaseTestCase(APIViewTestCase):
    filter_class = RecipientsFilterBackend

    def _make_request(self, **kwargs):
        factory = RequestFactory()
        request = factory.get(reverse('recipients'))
        request.query_params = kwargs
        return request

    def _get_user_ids(self, users, transform=None):
        transform = transform or (lambda x: getattr(x, 'id'))
        return list(map(transform, users))


class FilterByReminderIdTestCase(BaseTestCase):
    def test_with_reminder_id_is_not_present(self):
        request = self._make_request()
        response = self.filter_class().filter_by_reminder_id(request)
        self.assertEqual(response, Q())

    def test_with_reminder_id(self):
        reminder_id = 1
        request = self._make_request(reminder_id=reminder_id)
        response = self.filter_class().filter_by_reminder_id(request)
        self.assertEqual(response, Q(reminders__id=reminder_id))


class ExcludeByReminderIdTestCase(BaseTestCase):
    def test_with_exclude_reminder_id_is_not_present(self):
        request = self._make_request()
        response = self.filter_class().exclude_by_reminder_id(request)
        self.assertEqual(response, Q())

    def test_with_exclude_reminder_id(self):
        reminder_id = 1
        request = self._make_request(exclude_reminder_id=reminder_id)
        response = self.filter_class().exclude_by_reminder_id(request)
        self.assertEqual(response, ~Q(reminders__id=reminder_id))


class GetQueryTestCase(BaseTestCase):
    def setUp(self):
        self.filter_instance = self.filter_class()

    def test_methods_was_called(self):
        with patch.object(self.filter_instance, 'filter_by_reminder_id', return_value=Q()) as mock:
            with patch.object(self.filter_instance, 'exclude_by_reminder_id', return_value=Q()) as mock1:
                request = self._make_request()
                self.assertEqual(self.filter_instance.get_query(request), Q())
                mock.assert_called_once_with(request)
                mock1.assert_called_once_with(request)

    def test_combine_queries(self):
        reminder_id = 1
        with patch.object(self.filter_instance, 'filter_by_reminder_id', return_value=Q(reminders__id=reminder_id)):
            return_value = ~Q(reminders__id=reminder_id)
            with patch.object(self.filter_instance, 'exclude_by_reminder_id', return_value=return_value):
                request = self._make_request()
                self.assertEqual(
                    self.filter_instance.get_query(request),
                    Q(reminders__id=reminder_id) & ~Q(reminders__id=reminder_id))


class FilterQuerysetTestCase(BaseTestCase):
    def test_get_query_was_called(self):
        filter_instance = self.filter_class()
        queryset = User.objects.all()
        with patch.object(filter_instance, 'get_query', return_value=Q()) as mock:
            request = self._make_request()
            self.assertQuerysetEqual(filter_instance.filter_queryset(request, queryset, None), queryset)
            mock.assert_called_once_with(request)

    def test_filtering_queryset(self):
        filter_instance = self.filter_class()
        queryset = User.objects.all()
        reminder_id = 1
        with patch.object(filter_instance, 'get_query', return_value=Q(reminders__id=reminder_id)):
            with patch.object(queryset, 'filter', return_value=queryset) as mock:
                request = self._make_request()
                self.assertQuerysetEqual(filter_instance.filter_queryset(request, queryset, None), queryset)
                mock.assert_called_once_with(Q(reminders__id=reminder_id))
