from django.test import TestCase, RequestFactory
from unittest.mock import MagicMock, patch
from django.urls import reverse
from django.db.models import Q

from users.models import User
from users.services import UsersService


class BaseTestCase(TestCase):
    service_class = UsersService

    def setUp(self):
        self.data = {
            'username': 'test',
            'email': 'test@test.test',
            'first_name': 'first name',
            'last_name': 'last name',
        }
        self.user_password = 'password12345678'
        self.user = User.objects.create(password=self.user_password, **self.data)


class GetByIdentityTestCase(BaseTestCase):
    def test_get_user_by_email(self):
        response_user = self.service_class.get_by_identity(self.user.email)
        self.assertEqual(self.user.id, response_user.id)

    def test_get_user_by_username(self):
        response_user = self.service_class.get_by_identity(self.user.username)
        self.assertEqual(self.user.id, response_user.id)

    def test_user_manager_was_called(self):
        with patch.object(self.service_class, 'model') as mock:
            with patch.object(mock, 'objects') as manager:
                first_method_mock = MagicMock(return_value=self.user)
                manager.filter = MagicMock(return_value=first_method_mock)
                self.service_class.get_by_identity(self.user.username)
                query = Q(username__iexact=self.user.username) | Q(email__iexact=self.user.username)
                manager.filter.assert_called_once_with(query)
                first_method_mock.first.assert_called_once()


class AuthenticateTestCase(BaseTestCase):
    def test_get_by_identity_was_called(self):
        with patch.object(self.service_class, 'get_by_identity', return_value=self.user) as mock:
            self.service_class.authenticate(self.user.username, self.user_password)
            mock.assert_called_once_with(self.user.username)

    def test_user_check_password_was_called(self):
        with patch.object(self.user, 'check_password', return_value=True) as user_mock:
            with patch.object(self.service_class, 'get_by_identity', return_value=self.user):
                self.service_class.authenticate(self.user.username, self.user_password)
                user_mock.assert_called_once_with(self.user_password)

    def test_user_not_found(self):
        with patch.object(self.service_class, 'get_by_identity', return_value=None):
            self.assertEqual(self.service_class.authenticate(self.user.username, self.user_password), None)

    def test_user_found_but_password_is_invalid(self):
        with patch.object(self.user, 'check_password', return_value=False):
            with patch.object(self.service_class, 'get_by_identity', return_value=self.user):
                self.assertEqual(self.service_class.authenticate(self.user.username, self.user_password), None)

    def test_user_found_and_password_is_valid(self):
        with patch.object(self.user, 'check_password', return_value=True):
            with patch.object(self.service_class, 'get_by_identity', return_value=self.user):
                self.assertEqual(self.service_class.authenticate(self.user.username, self.user_password), self.user)


class LoginUserTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        factory = RequestFactory()
        self.request = factory.get(reverse('sign_in'))

    def test_user_last_login_was_updated(self):
        if self.user.last_login is None:
            self.service_class.login_user(self.user, self.request)
            self.assertTrue(self.user.last_login is not None)

        last_login = self.user.last_login
        self.service_class.login_user(self.user, self.request)
        self.assertTrue(last_login < self.user.last_login)
