from rest_framework_simplejwt.tokens import RefreshToken
from unittest.mock import MagicMock, PropertyMock
from django.test import TestCase

from users.models import User
from users.serializers import SignUpSerializer, UserSerializer


class BaseTestCase(TestCase):
    def setUp(self):
        self.data = {
            'username': 'test',
            'email': 'test@test.test',
            'password': 'password12345678',
            'password_confirmation': 'password12345678',
        }

    def _create_user(self):
        data = self.data.copy()
        data.pop('password_confirmation')
        return User.objects.create(**data)

    def _check_field_error_presents(self, field, message, code='invalid', use_double_quotes=False):
        serializer = SignUpSerializer(data=self.data)
        self.assertFalse(serializer.is_valid())
        self.assertTrue(field in serializer.errors)
        string = f'\"{message}\"' if use_double_quotes else f'\'{message}\''
        self.assertEqual(str(serializer.errors[field]), f"[ErrorDetail(string={string}, code='{code}')]")


class FieldsPresentsTestCase(BaseTestCase):
    def _check_field_presents(self, field):
        self.data.pop(field)
        self._check_field_error_presents(field, 'This field is required.', code='required')

    def test_identity_field_presents(self):
        self._check_field_presents('username')

    def test_email_field_presents(self):
        self._check_field_presents('email')

    def test_password_field_presents(self):
        self._check_field_presents('password')

    def test_password_confirmation_field_presents(self):
        self._check_field_presents('password_confirmation')


class ValidationTestCase(BaseTestCase):
    def _check_field_uniqueness(self, field, message):
        self._create_user()
        self._check_field_error_presents(field, message, 'unique')

    def test_with_too_common_password_identity(self):
        self.data['password'] = 'password'
        self.data['password_confirmation'] = 'password'
        self._check_field_error_presents('password', 'This password is too common.', code='password_too_common')

    def test_with_unmatched_passwords_identity(self):
        self.data['password'] = 'password'
        self._check_field_error_presents('password_confirmation', "Passwords don't match", use_double_quotes=True)

    def test_username_uniqueness(self):
        self._check_field_uniqueness('username', 'A user with that username already exists.')

    def test_email_uniqueness(self):
        self._check_field_uniqueness('email', 'user with this email address already exists.')

    def test_with_valid_data(self):
        serializer = SignUpSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())


class ToRepresentationTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.user = self._create_user()

    def _prepare_to_call_to_representation(self):
        serializer = SignUpSerializer()
        token = RefreshToken.for_user(self.user)
        type(token).access_token = PropertyMock(return_value=token.access_token)
        serializer.token_service.for_user = MagicMock(return_value=token)
        return serializer, token

    def test_token_service_was_called(self):
        serializer, token = self._prepare_to_call_to_representation()
        serializer.to_representation(self.user)
        serializer.token_service.for_user.assert_called_once_with(self.user)

    def test_to_represent_return_value(self):
        serializer, token = self._prepare_to_call_to_representation()
        response = serializer.to_representation(self.user)
        self.assertEqual(response['refresh'], str(token))
        self.assertEqual(response['access'], str(token.access_token))
        self.assertDictEqual(response['user'], UserSerializer(self.user).data)


class CreateUserTestCase(BaseTestCase):
    def test_user_creation(self):
        serializer = SignUpSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())
        count = int(User.objects.count())
        user = serializer.save()
        self.assertEqual(User.objects.count(), count + 1)
        self.assertTrue(user.check_password(self.data['password']))
