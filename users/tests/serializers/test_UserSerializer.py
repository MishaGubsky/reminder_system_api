from django.test import TestCase

from users.models import User
from users.serializers import UserSerializer


class BaseTestCase(TestCase):
    serializer_class = UserSerializer

    def setUp(self):
        self.data = {
            'username': 'test',
            'email': 'test@test.test',
            'first_name': 'first name',
            'last_name': 'last name',
        }


class ToInternalValueTestCase(BaseTestCase):
    def test_email_lower_case(self):
        email = 'TesT@Test.TEST'
        self.data['email'] = email
        serializer = self.serializer_class()
        response = serializer.to_internal_value(self.data)
        self.assertEqual(response['email'], email.lower())


class ToRepresentationTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.user = User.objects.create(**self.data)
        self.serializer = self.serializer_class()

    def test_fields_present(self):
        data = self.serializer.to_representation(self.user)
        fields = ('id', 'username', 'email', 'first_name', 'last_name',)
        for field in fields:
            self.assertTrue(field in data)
            self.assertEqual(data.get(field), getattr(self.user, field))

        self.assertTrue('full_name' in data)
        self.assertEqual(data.get('full_name'), self.user.get_full_name())
