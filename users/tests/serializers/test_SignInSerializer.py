from rest_framework_simplejwt.tokens import RefreshToken
from unittest.mock import MagicMock, PropertyMock, patch
from django.test import TestCase

from users.models import User
from users.serializers import SignInSerializer, UserSerializer


class BaseTestCase(TestCase):
    def setUp(self):
        self.data = {
            'identity': 'test',
            'password': 'password',
        }


class BaseWithUserCreationTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.user = User(username=self.data['identity'])
        self.user.set_password(self.data['password'])
        self.user.save()


class FieldsPresentsTestCase(BaseTestCase):
    def _check_field_presents(self, field):
        self.data.pop(field)
        serializer = SignInSerializer(data=self.data)
        self.assertFalse(serializer.is_valid())
        self.assertTrue(field in serializer.errors)
        self.assertEqual(
            str(serializer.errors[field]), "[ErrorDetail(string='This field is required.', code='required')]")

    def test_identity_field_presents(self):
        self._check_field_presents('identity')

    def test_password_field_presents(self):
        self._check_field_presents('password')


class AuthenticationTestCase(BaseWithUserCreationTestCase):
    def _check_invalid_field_presents(self, field):
        self.data[field] += '1'
        serializer = SignInSerializer(data=self.data)
        self.assertFalse(serializer.is_valid())
        self.assertTrue('password' in serializer.errors)
        self.assertEqual(
            str(serializer.errors['password']),
            "[ErrorDetail(string='Login or password is incorrect', code='invalid')]")

    def test_with_invalid_identity(self):
        self._check_invalid_field_presents('identity')

    def test_with_invalid_password(self):
        self._check_invalid_field_presents('password')

    def test_with_valid_data(self):
        serializer = SignInSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.user.id, self.user.id)

    def test_users_service_was_called(self):
        serializer = SignInSerializer(data=self.data)
        with patch.object(SignInSerializer, 'users_service') as mock:
            mock.authenticate = MagicMock(return_value=self.user)
            self.assertTrue(serializer.is_valid())
            mock.authenticate.assert_called_once_with(self.data['identity'], self.data['password'])

    def test_return_validate_method(self):
        serializer = SignInSerializer()
        self.assertEqual(serializer.validate(self.data), self.data)

    def test_user_was_saved(self):
        serializer = SignInSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.user.id, self.user.id)


class ToRepresentationTestCase(BaseWithUserCreationTestCase):
    def _prepare_to_call_to_representation(self):
        serializer = SignInSerializer(data=self.data)
        serializer.user = self.user
        token = RefreshToken.for_user(self.user)
        type(token).access_token = PropertyMock(return_value=token.access_token)
        serializer.token_service.for_user = MagicMock(return_value=token)
        return serializer, token

    def test_token_service_was_called(self):
        serializer, token = self._prepare_to_call_to_representation()
        serializer.to_representation(self.data)
        serializer.token_service.for_user.assert_called_once_with(self.user)

    def test_to_represent_return_value(self):
        serializer, token = self._prepare_to_call_to_representation()
        response = serializer.to_representation(self.data)
        self.assertEqual(response['refresh'], str(token))
        self.assertEqual(response['access'], str(token.access_token))
        self.assertDictEqual(response['user'], UserSerializer(self.user).data)


class FullProcessTestCase(BaseWithUserCreationTestCase):
    def test_full_process(self):
        serializer = SignInSerializer(data=self.data)
        serializer.user = self.user
        token = RefreshToken.for_user(self.user)
        type(token).access_token = PropertyMock(return_value=token.access_token)
        serializer.token_service.for_user = MagicMock(return_value=token)
        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.data['refresh'], str(token))
        self.assertEqual(serializer.data['access'], str(token.access_token))
        self.assertDictEqual(serializer.data['user'], UserSerializer(self.user).data)
