from django.test import TestCase

from users.models import User
from users.serializers import UserEditSerializer, UserSerializer


class BaseTestCase(TestCase):
    serializer_class = UserEditSerializer

    def setUp(self):
        self.data = {
            'username': 'test',
            'email': 'test@test.test',
            'first_name': 'first name',
            'last_name': 'last name',
        }
        self.user_password = 'password12345678'

        self.user = User(**self.data)
        self.user.set_password(self.user_password)
        self.user.save()

    def _check_field_error_presents(self, data, field, message, code='invalid', use_double_quotes=False):
        serializer = self.serializer_class(self.user, data=data, partial=True)
        self.assertFalse(serializer.is_valid())
        self.assertTrue(field in serializer.errors)
        string = f'\"{message}\"' if use_double_quotes else f'\'{message}\''
        self.assertEqual(str(serializer.errors[field]), f"[ErrorDetail(string={string}, code='{code}')]")


class ValidationTestCase(BaseTestCase):
    def test_validate_password_without_current_password(self):
        data = {
            'password': 'password12345678'
        }
        self._check_field_error_presents(data, 'current_password', 'This field is required.')

    def test_with_too_common_password(self):
        data = {
            'password': 'password',
            'current_password': self.user_password,
        }
        self._check_field_error_presents(data, 'password', 'This password is too common.', code='password_too_common')

    def test_validate_current_password(self):
        data = {
            'current_password': 'password',
        }
        self._check_field_error_presents(data, 'current_password', 'Invalid password')

    def test_with_unmatched_passwords_identity(self):
        data = {
            'password': 'password',
            'password_confirmation': 'password1',
            'current_password': self.user_password,
        }
        self._check_field_error_presents(
            data, 'password_confirmation', "Passwords don't match", use_double_quotes=True)

    def test_username_uniqueness(self):
        data = {'username': 'test_username'}
        User.objects.create(username=data['username'], email='test_email@test.test')
        self._check_field_error_presents(data, 'username', 'A user with that username already exists.', code='unique')

    def test_email_uniqueness(self):
        data = {'email': 'test_email@test.test'}
        User.objects.create(username='test_username', email=data['email'])
        self._check_field_error_presents(data, 'email', 'user with this email address already exists.', code='unique')


class ToRepresentationTestCase(BaseTestCase):
    def test_to_represent_return_value(self):
        serializer = self.serializer_class()
        response = serializer.to_representation(self.user)
        self.assertDictEqual(response, UserSerializer(self.user).data)


class UpdateUserTestCase(BaseTestCase):
    def test_password_update(self):
        data = {
            'password': 'password12345678',
            'current_password': self.user_password,
            'password_confirmation': 'password12345678'
        }
        serializer = self.serializer_class(self.user, data=data, partial=True)
        self.assertTrue(serializer.is_valid())
        updated_user = serializer.save()
        self.assertTrue(updated_user.check_password(data['password']))

    def test_user_update(self):
        user_id = self.user.id
        data = {
            'username': 'test1',
            'email': 'test1@test.test',
            'first_name': 'first1 name',
            'last_name': 'last1 name',
        }
        serializer = self.serializer_class(self.user, data=data, partial=True)
        self.assertTrue(serializer.is_valid())
        updated_user = serializer.save()
        self.assertEqual(updated_user.id, user_id)
        for field in data.keys():
            self.assertEqual(getattr(updated_user, field), data[field])
