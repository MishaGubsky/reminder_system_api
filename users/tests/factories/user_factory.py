import factory
from faker import Factory

from users.models import User

faker = Factory.create()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    first_name = faker.first_name()
    last_name = faker.last_name()
    username = factory.Sequence(lambda n: faker.user_name() + str(n))
    email = factory.Sequence(lambda n: faker.email(str(n) + faker.free_email_domain()))
