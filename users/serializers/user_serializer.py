from rest_framework import serializers
from django.http import QueryDict

from users.models import User


class UserSerializer(serializers.ModelSerializer):
    full_name = serializers.CharField(source='get_full_name', read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name', 'full_name',)

    def to_internal_value(self, data):
        if data.get('email', None):
            data = data.dict() if isinstance(data, QueryDict) else data
            data['email'] = data.get('email').lower()

        return super().to_internal_value(data)
