from django.contrib.auth import password_validation
from django.utils.translation import ugettext as _
from rest_framework import serializers

from .user_serializer import UserSerializer


class UserEditSerializer(UserSerializer):
    password = serializers.CharField(write_only=True, required=False, allow_null=True)
    current_password = serializers.CharField(write_only=True, required=False, allow_null=True)
    password_confirmation = serializers.CharField(write_only=True, required=False, allow_null=True)

    class Meta(UserSerializer.Meta):
        fields = UserSerializer.Meta.fields + ('password', 'current_password', 'password_confirmation',)

    def validate(self, data):
        if data.get('password') and not data.get('current_password'):
            raise serializers.ValidationError({'current_password': _('This field is required.')})

        return data

    def validate_password(self, value):
        password_validation.validate_password(value)
        return value

    def validate_current_password(self, value):
        if self.instance and not self.instance.check_password(value):
            raise serializers.ValidationError(_('Invalid password'))

        return value

    def validate_password_confirmation(self, value):
        password = self.initial_data.get('password')

        if password and value != password:
            raise serializers.ValidationError(_('Passwords don\'t match'))

        return value

    def update(self, instance, validated_data):
        password = validated_data.pop('password', None)
        if password:
            instance.set_password(password)

        return super().update(instance, validated_data)
