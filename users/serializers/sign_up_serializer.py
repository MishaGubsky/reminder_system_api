from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth import password_validation
from django.utils.translation import ugettext as _
from rest_framework import serializers

from users.models import User
from .user_serializer import UserSerializer


class SignUpSerializer(UserSerializer):
    password = serializers.CharField(write_only=True)
    password_confirmation = serializers.CharField(write_only=True)

    token_service = RefreshToken

    class Meta(UserSerializer.Meta):
        fields = UserSerializer.Meta.fields + ('password', 'password_confirmation',)

    def to_representation(self, instance):
        refresh = RefreshToken.for_user(instance)
        return {
            'user': super().to_representation(instance),
            'access': str(refresh.access_token),
            'refresh': str(refresh),
        }

    def validate_password(self, value):
        password_validation.validate_password(value)
        return value

    def validate_password_confirmation(self, value):
        password = self.initial_data.get('password')

        if password and value != password:
            raise serializers.ValidationError(_('Passwords don\'t match'))

        return value

    def create(self, validated_data):
        validated_data.pop('password_confirmation')
        return User.objects.create_user(**validated_data)
