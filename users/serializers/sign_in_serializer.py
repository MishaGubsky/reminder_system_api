from rest_framework_simplejwt.tokens import RefreshToken
from django.utils.translation import ugettext as _
from rest_framework import serializers

from .user_serializer import UserSerializer
from users.services import UsersService


class SignInSerializer(serializers.Serializer):
    users_service = UsersService
    token_service = RefreshToken

    identity = serializers.CharField()
    password = serializers.CharField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None

    def to_representation(self, instance):
        refresh = self.token_service.for_user(self.user)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
            'user': UserSerializer(self.user).data
        }

    def validate(self, data):
        identity = data.get('identity')
        password = data.get('password')
        self.user = self.users_service.authenticate(identity, password)

        if not self.user:
            raise serializers.ValidationError({'password': _('Login or password is incorrect')})

        return data
