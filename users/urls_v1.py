from django.conf.urls import url
from rest_framework_simplejwt.views import TokenRefreshView

from users.views.auth import SignUpView, SignInView
from users.views import CurrentUserView, RecipientsListView

urlpatterns = [
    url(r'^auth/sign_up/$', SignUpView.as_view(), name='sign_up'),
    url(r'^auth/sign_in/$', SignInView.as_view(), name='sign_in'),
    url(r'^auth/refresh_token/', TokenRefreshView.as_view(), name='refresh_token'),
    url(r'^current_user/$', CurrentUserView.as_view(), name='current_user'),
    url(r'recipients/$', RecipientsListView.as_view(), name='recipients'),
]
