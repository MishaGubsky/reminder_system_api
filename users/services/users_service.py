from django.contrib.auth.models import update_last_login
from django.db.models import Q

from users.models import User


class UsersService:
    model = User

    @classmethod
    def get_by_identity(cls, identity):
        query = Q(username__iexact=identity) | Q(email__iexact=identity)
        return cls.model.objects.filter(query).first()

    @classmethod
    def authenticate(cls, identity, password):
        user = cls.get_by_identity(identity)
        if user and user.check_password(password):
            return user

    @classmethod
    def login_user(cls, user, request):
        update_last_login(request, user)
