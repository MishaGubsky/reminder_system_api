from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext as _
from django.db import models


class User(AbstractUser):
    email = models.EmailField(_('email address'), unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
