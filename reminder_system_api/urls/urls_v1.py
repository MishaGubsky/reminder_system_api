from django.conf.urls import include, url

urlpatterns = [
    url(r'^', include('users.urls_v1')),
    url(r'^', include('reminders.urls_v1')),
]
