from django.core.mail import EmailMessage
from django.core import mail
from smtplib import SMTPException
from django.conf import settings

from reminder_system_api.celery import app as celery_app


@celery_app.task
def send_message(receivers, subject, body):
    try:
        msg = EmailMessage(subject, body, to=receivers, from_email=settings.EMAIL_FROM)
        msg.content_subtype = 'html'
        msg.send(fail_silently=False)
        return True
    except SMTPException as e:
        print(e)
        return False


class BaseMailer:
    @classmethod
    def _send_email(cls, receivers, subject, body, delay=False):
        if delay:
            send_message.delay(receivers, subject, body)
            return True

        return send_message(receivers, subject, body)

    @classmethod
    def _send_mass_email(cls, data, from_email=settings.EMAIL_FROM):
        try:
            with mail.get_connection() as connection:
                for email_data in data:
                    msg = mail.EmailMessage(
                        email_data.get('subject'),
                        email_data.get('body'),
                        to=email_data.get('receivers'),
                        from_email=from_email,
                        connection=connection
                    )
                    msg.content_subtype = 'html'
                    msg.send(fail_silently=False)
            return True
        except SMTPException:
            return False
