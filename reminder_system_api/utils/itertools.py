from django.core.paginator import Paginator


def batches(sequence, step):
    for i in range(0, len(sequence), step):
        yield sequence[i:i+step]


def chunk_queryset_iterator(queryset, chunk_size=10000, yield_chunk=False):
    paginator = Paginator(queryset, chunk_size)
    for page in range(1, paginator.num_pages + 1):
        if yield_chunk:
            yield paginator.page(page).object_list
        else:
            for obj in paginator.page(page).object_list:
                yield obj
