from rest_framework import pagination


class CustomPageNumberPagination(pagination.PageNumberPagination):
    def get_next_link(self):
        if not self.page.has_next() or str(self.page.number) in self.last_page_strings:
            return None

        return {'page': self.page.next_page_number()}

    def get_previous_link(self):
        if not self.page.has_previous():
            return None

        page_number = self.page.previous_page_number()
        return {'page': page_number}
