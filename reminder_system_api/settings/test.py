from .base import *
import os

DATABASES = {
  'default': {
    'ENGINE': 'django.db.backends.mysql',
    'HOST': os.environ["DB_HOST"],
    'PORT': os.environ["DB_PORT"],
    'USER': os.environ["TEST_DB_USER"],
    'PASSWORD': os.environ["TEST_DB_PASSWORD"],
    'NAME': os.environ["TEST_DB_NAME"],
  }
}
